#include<stdio.h>
#include<stdlib.h>
#include <time.h>

#define cases 3
typedef void (*vmethod)(void* this);
vmethod *vtable_matrix_class;
vmethod *vtable_vec_class;

typedef struct matrix {
	int rows;
	int cols;
	int *array;
        vmethod *vtable_ptr;
	void (*sum)(void* this, void* mat);
	void (*mul)(void* this, void* mat);
	

} matrix;

typedef struct vector_ {
	matrix mat_vec;
	int min;
	int max;
	vmethod *vtable_ptr;
} vector;

void matrix_norm( matrix *this) {
	
	int i,j,max,l1_norm;
	

	for(i=0; i<this->cols; i++){
		for(j=0; j<this->rows;j++){
  			max +=this->array[i+j*this->cols];
	}
		if(max>l1_norm){
		l1_norm=max;	
	}
	max=0;
	} 
	
	printf("matrix l1_norm: %d\n",l1_norm);
}
void vec_sum( matrix* this, matrix* b) {
	
		int i,j;
		printf("vec_sum:\n");
		for(j=0; j<this->rows;j++){
  			this->array[i*this->rows+j]=this->array[i*this->rows+j]+ b->array[i*this->rows+j];
  			printf("%d ", this->array[i*this->rows+j]);
		}
  	 	//printf("\n");
 	
}


void vec_norm( matrix *this){
	int l1_norm=0,i=0,max=0;
	

	for(i=0; i<this->rows;i++){
		max+=this->array[i*this->rows+i];
                l1_norm=max;
	}
	printf("vector l1_norm: %d\n",l1_norm );
	}

void multiplication( matrix* this, matrix* b) { 
	
		int i,j,k,flag;
	           
		for (i=0; i<this->rows; i++){
			for(j=0; j<b->cols; j++){
				for(k=0; k<this->cols; k++){
			
					flag += this->array[i*this->cols+k] * b->array[k*b->cols+j];
				}
			
				
				
				printf("%d ",flag );
				flag=0;

			
			}
			printf("\n");
		}
	

}
void matrix_sum( matrix* this, matrix* b) {
	
	int i,j;
	printf("sum:\n");
	for(i=0; i<this->rows; i++){
		for(j=0; j<this->cols;j++){
  			this->array[i*this->cols+j]=this->array[i*this->cols+j]+ b->array[i*this->cols+j];
  			printf("%d\t", this->array[i*this->cols+j]);
		}
  		printf("\n");
	} 
	

}



void matrix_init(matrix* this, int rows, int cols){
this->array= malloc(sizeof(int)*rows*cols);
	this->sum = &matrix_sum;
	this->mul = &multiplication;
	this->rows=rows;
	this->cols=cols;
	
	int i,j;
	
	for(i=0; i<rows; i++){
		for(j=0; j<cols;j++){
			this->array[i*this->cols+j]=rand()%10;
			printf("%d ", this->array[i*this->cols+j]);
		}
 		printf("\n");
	}

}

void vec_init(vector* this, int rows){
int i,j,max,min;
this->mat_vec.array= malloc(sizeof(int)*rows*this->mat_vec.cols);
	this->mat_vec.cols=1;
	this->mat_vec.sum = &vec_sum;
	this->mat_vec.mul = &multiplication;
	this->mat_vec.rows=rows;

	
	
	
	for(j=0; j<rows;j++){
		this->mat_vec.array[i*this->mat_vec.rows+j]=rand()%10;
		printf("%d ", this->mat_vec.array[i*this->mat_vec.rows+j]);
		if(this->mat_vec.array[i*this->mat_vec.rows+j]>max){
			max=this->mat_vec.array[i*this->mat_vec.rows+j];
		}

	}
 	
	
	min=this->mat_vec.array[0];
	
	for(j=0; j<rows;j++){
		if(this->mat_vec.array[i*this->mat_vec.rows+j]<min){
			min=this->mat_vec.array[i*this->mat_vec.rows+j];
		}
	}
	

}


int main(){

	vtable_matrix_class=(vmethod*)malloc(sizeof(vmethod));
	vtable_vec_class=(vmethod*)malloc(sizeof(vmethod));
	vtable_matrix_class[0]=&matrix_norm;
	vtable_vec_class[0]=&vec_norm;


	int news=0;
	printf("1 for matrix addition,\n 2 for vector addition \n 3 for matrix multiplication \n 4 for matrix_l1 norm \n 5 for vector_l1 norm ");
	scanf("%d",&news);
	
		switch(news)
{

          case 1:{
	                matrix m1,m2;
			matrix_init(&m1, cases,cases);
			matrix_init(&m2, cases,cases);
			m1.sum(&m1, &m2);
		        break;
}
          case 2:	
		       {
vector v1,v2;
			vec_init(&v1,cases);
			vec_init(&v2,cases);
			v1.mat_vec.sum(&v1,&v2);
			
		     break;
}
        case 3:{
                         matrix m1,m2;
			matrix_init(&m1, cases,cases);
			matrix_init(&m2, cases,cases);
			m1.mul(&m1, &m2);
			
                      break;
}

		
        case 4:
	                  {  matrix m1;
			matrix_init(&m1, cases,cases);
			m1.vtable_ptr=vtable_matrix_class;
			m1.vtable_ptr[0](&m1);
			
		        break;
}
        case 5:
			{
                         vector v1;
			vec_init(&v1,cases);
			v1.vtable_ptr=vtable_vec_class;
	                v1.vtable_ptr[0](&v1);
                        break;
		       
}
		
	
}
}

